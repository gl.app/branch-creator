# Branch Creator

## Summary/Description

![screenshot](./images/branch-creator.png "Branch Creator")

This app provides a mechanism for easily creating bitbucket feature branches for Rally artifacts (Portfolio Item Features, associated user stories and defects) using a custom list.  

To create a branch, click on the Branch icon on the left of the grid.

The created branch can be saved into a custom branch link text field on the artifact it is launched from by pasting the url for the new
branch into the dialog that is presented when the branch is launched.  

A custom text field is required to accommodate multiple branches in a single artifact.  The custom field is set in the app settings.
If the set custom branch link field does not exist for the artifact type shown, then the branch icon will not be available on the
left bar of the grid.  

Branch links can only be removed or edited by editing the field in the detail view for the artifact.

Note that there is no enforcement of the formatting in edited branch link fields.  Editing a branch link field and overwriting or removing the branch link may result in loss or corruption of the branch link.

** NOTE:  This app is currently hard-coded to display artifacts of type PortfolioItem/Feature and Defect.  User stories are accessible as children of the PortfolioItem/Features.  If the lowest level portfolio item is not a Feature, or if to display other artifact types, the default settings in the App.js must be modified.  Future releases may allow for the ability to select specific artifact types and accommodate Portfolio Items with non-default naming.  

## App Settings
![screenshot](./images/app-settings.png "Branch Creator App Settings")

##### Branch Link(s) Field
This field is the field that branch links will be stored in.  It must be a field of type TEXT and it must also exist on every artifact
type that you would like to create branches for.  It must (at the least) exist on the Portfolio Item type.  

##### Link to Create Branch
This represents the link to create a branch in the remote repository.  The format for Bitbucket is:
http://<Bitbucket Server Address>:<Port>/plugins/servlet/create-branch?issueKey={id}&issueType=Story&issueSummary={name}

In this link, keep the {id} and {name} variables in the link to default the branch naming.  The {id} variable will be replaced by the
formatted ID for the selected artifact and the {name} variable will be replaced by the artifact name (spaces replaced with dashes). Note that
in Bitbucket a branch cannot be more than 100 characters.  

##### Artifact Query
Enter a valid Rally query that should limit what artifacts are displayed.  If no query is entered here, then all PortfolioItem/Features and Defects in the current scope will be loaded.  Note that the query must apply to both artifact types (PortfolioItem/Features and Defects).  If it does not, only the 
artifact type that it applies to will be displayed.  All story children of Portfolio Items will be displayed under the appropriate Portfolio Item
regardless of this query.  

### First Load

If you've just downloaded this from github and you want to do development,
you're going to need to have these installed:

 * node.js
 * grunt-cli
 * grunt-init

Since you're getting this from github, we assume you have the command line
version of git also installed.  If not, go get git.

If you have those three installed, just type this in the root directory here
to get set up to develop:

  npm install

#### Deployment & Tests

If you want to use the automatic deployment mechanism, be sure to use the
**makeauth** task with grunt to create a local file that is used to connect
to Rally.  This resulting auth.json file should NOT be checked in.

### Structure

  * src/javascript:  All the JS files saved here will be compiled into the
  target html file
  * src/style: All of the stylesheets saved here will be compiled into the
  target html file
  * test/fast: Fast jasmine tests go here.  There should also be a helper
  file that is loaded first for creating mocks and doing other shortcuts
  (fastHelper.js) **Tests should be in a file named <something>-spec.js**
  * test/slow: Slow jasmine tests go here.  There should also be a helper
  file that is loaded first for creating mocks and doing other shortcuts
  (slowHelper.js) **Tests should be in a file named <something>-spec.js**
  * templates: This is where templates that are used to create the production
  and debug html files live.  The advantage of using these templates is that
  you can configure the behavior of the html around the JS.
  * config.json: This file contains the configuration settings necessary to
  create the debug and production html files.  
  * package.json: This file lists the dependencies for grunt
  * auth.json: This file should NOT be checked in.  This file is needed for deploying
  and testing.  You can use the makeauth task to create this or build it by hand in this'
  format:
    {
        "username":"you@company.com",
        "password":"secret",
        "server": "https://rally1.rallydev.com"
    }

### Usage of the grunt file
#### Tasks

##### grunt debug

Use grunt debug to create the debug html file.  You only need to run this when you have added new files to
the src directories.

##### grunt build

Use grunt build to create the production html file.  We still have to copy the html file to a panel to test.

##### grunt test-fast

Use grunt test-fast to run the Jasmine tests in the fast directory.  Typically, the tests in the fast
directory are more pure unit tests and do not need to connect to Rally.

##### grunt test-slow

Use grunt test-slow to run the Jasmine tests in the slow directory.  Typically, the tests in the slow
directory are more like integration tests in that they require connecting to Rally and interacting with
data.

##### grunt deploy

Use grunt deploy to build the deploy file and then install it into a new page/app in Rally.  It will create the page on the Home tab and then add a custom html app to the page.  The page will be named using the "name" key in the config.json file (with an asterisk prepended).

You can use the makeauth task to create this file OR construct it by hand.  Caution: the
makeauth task will delete this file.

The auth.json file must contain the following keys:
{
    "username": "fred@fred.com",
    "password": "fredfredfred",
    "server": "https://us1.rallydev.com"
}

(Use your username and password, of course.)  NOTE: not sure why yet, but this task does not work against the demo environments.  Also, .gitignore is configured so that this file does not get committed.  Do not commit this file with a password in it!

When the first install is complete, the script will add the ObjectIDs of the page and panel to the auth.json file, so that it looks like this:

{
    "username": "fred@fred.com",
    "password": "fredfredfred",
    "server": "https://us1.rallydev.com",
    "pageOid": "52339218186",
    "panelOid": 52339218188
}

On subsequent installs, the script will write to this same page/app. Remove the
pageOid and panelOid lines to install in a new place.  CAUTION:  Currently, error checking is not enabled, so it will fail silently.

##### grunt watch

Run this to watch files (js and css).  When a file is saved, the task will automatically build, run fast tests, and deploy as shown in the deploy section above.

##### grunt makeauth

This task will create an auth.json file in the proper format for you.  **Be careful** this will delete any existing auth.json file.  See **grunt deploy** to see the contents and use of this file.

##### grunt --help  

Get a full listing of available targets.
