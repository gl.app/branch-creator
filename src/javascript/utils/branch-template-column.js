Ext.define('gl.app.branch-creator.BranchTemplateColumn', {
    extend: 'Ext.grid.column.Template',
    alias: ['widget.branchtemplatecolumn'],

    align: 'right',

    initComponent: function(){
        var me = this;

        me.tpl = new Ext.XTemplate('<tpl><div class="{[this.getBranchClass(values)]}" style="cursor:pointer;text-align:right;">{[this.getBranchLink(values)]}</div></tpl>',{
            branchField: me.branchField,
            getBranchLink: function(values){
                if (values[this.branchField] === null){
                    return "";
                }
                return this.branchField;
            },
            getBranchClass: function(values){
                if (values[this.branchField]){
                    return "no-class";
                }
                return "icon-branch";
            }

        });
        me.hasCustomRenderer = true;
        me.callParent(arguments);
    },
    // getValue: function(){
    //     return values[this.costField] || 0;
    // },
    defaultRenderer: function(value, meta, record) {
        var data = Ext.apply({}, record.data); //, record.getAssociatedData());
        return this.tpl.apply(data);
    }
});
