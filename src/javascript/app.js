Ext.define("gl.app.branch-creator", {
    extend: 'Rally.app.App',
    componentCls: 'app',
    logger: new CArABU.technicalservices.Logger(),
    defaults: { margin: 10 },

    integrationHeaders: {
        name: "gl.app.branch-creator"
    },

    config: {
       defaultSettings: {
          artifactTypes: ["PortfolioItem/Feature","HierarchicalRequirement","Defect"],
          branchField: 'c_BitbucketBranches',
          createBranchLink: 'http://52.24.60.130:7990/plugins/servlet/create-branch?issueKey={id}&issueType=Story&issueSummary={name}',
          queryString: '',
          branchDelimeter: '<br/>',
          promptForBranchLink: false
       }
    },

    launch: function() {

        this.logger.setSaveForLater(this.getSetting('saveLog'));
        if (this._validateSettings()){
            this._buildGridStore();
        }
    },

    _validateSettings: function(){
      this.logger.log('_validateSettings');
      //TODO: write validation
        return true;
    },

    _buildGridStore: function(){
      var modelNames = this._getArtifactTypes(),
          branchLinkField = this.getBranchLinkField();

      Ext.create('Rally.data.wsapi.TreeStoreBuilder').build({
          models: modelNames,
          //autoLoad: true,
          fetch: ['Name','Project',branchLinkField],
          enableHierarchy: true
       }).then({
           success: this._addGrid,
           scope: this
      });

    },

    _addGrid: function(store){

        var gridBoard = this.down('#grid_board');
        if (gridBoard){
           gridBoard.destroy();
        }

        var promptForBranchLink = this.getPromptForBranchLink();

        var me = this,
            modelNames = this._getArtifactTypes(),
            branchField = this.getBranchLinkField(),
            branchLink = this.getCreateBranchLink(),
            branchDelimeter = this.getBranchDelimeter(),
            filter = this.getQueryString(); //"http://52.24.60.130:7990/plugins/servlet/create-branch?issueKey=" + selectedRecord.get('FormattedID') + "&issueType=Story&issueSummary=" + branchName;

        var columnCfgs = [{
          dataIndex: 'Name',
          flex: 1
        },{
          dataIndex: 'Project',
          flex: 1
        },{
          dataIndex: branchField,
          flex: 3

        }];

        this.add({
                xtype: 'rallygridboard',
                context: this.getContext(),
                modelNames: modelNames,
                toggleState: 'grid',
                stateful: false,
                plugins: [{
                  ptype: 'rallygridboardfieldpicker',
                  headerPosition: 'left',
                  modelNames: modelNames,
                  stateful: true,
                  stateId: this.getContext().getScopedStateId('state-branchLinkColumns')
                },{
                    ptype: 'rallygridboardinlinefiltercontrol',
                    inlineFilterButtonConfig: {
                        stateful: true,
                        stateId: this.getContext().getScopedStateId('state-branchLinkFilters'),
                        modelNames: modelNames,
                        inlineFilterPanelConfig: {
                            quickFilterPanelConfig: {
                                defaultFields: [
                                    'ArtifactSearch',
                                    'Owner',
                                    'ModelType'
                                ]
                            }
                        }
                    }
                }],
                gridConfig: {
                    store: store,
                    height: this.getHeight(),
                    storeConfig: {
                        pageSize: 200,
                        filters: filter
                    },
                    enableRanking: false,
                    enableBulkEdit: false,
                    columnCfgs: columnCfgs,
                    enableEditing: false,
                    shouldShowRowActionsColumn: true,
                    rowActionColumnConfig: {
                      _renderGearIcon: function(value, metaData, record) {
                          me.logger.log('branch ', record.get('FormattedID'), record.get(branchField));
                          if (record.hasField(branchField)){
                            return '<div class="row-action-icon icon-predecessor"/>';
                          }
                          return '';
                      },
                      _showMenu: function(view, el) {

                           var selectedRecord = view.getRecord(Ext.fly(el).parent("tr"));
                           var branchName = selectedRecord.get('Name');
                          //http://52.24.60.130:7990/plugins/servlet/create-branch?issueKey=F4&issueType=Story&issueSummary=Lone+Story
                          //Note branch names are limited to 100 characters.
                          branchLink = branchLink.replace('{id}', selectedRecord.get('FormattedID')).replace('{name}',selectedRecord.get('Name'));
                          var w = window.open(encodeURI(branchLink));
                          me.logger.log(w, w.location);

                          if (!promptForBranchLink){
                             return;
                          }

                          var dlg = Ext.create('Rally.ui.dialog.ConfirmDialog', {
                             autoShow: true,
                             draggable: true,
                             width: 400,
                             title: 'Branch Information',
                             titleIconHtml: '<span aria-hidden="true" class="icon-predecessor"></span> ',
                             items: [
                                {
                                    xtype: 'component',
                                    itemId: 'confirmMsg',
                                    cls: 'confirmMessage'
                                },{
                                  xtype     : 'textareafield',
                                  grow      : true,
                                  itemId      : 'branchLink',
                                  anchor    : '100%',
                                  width: '90%',
                                  padding: 10
                                }
                            ],
                              message: 'Please paste the branch link for [' + selectedRecord.get('FormattedID') + '-' + branchName + '] here:',
                              confirmLabel: 'Save',
                              listeners: {
                                  confirm: function(){
                                    if (this.down('#branchLink')){
                                        var branch = this.down('#branchLink').getValue();
                                        var currentBranches = selectedRecord.get(branchField),
                                           currentBranchArray = [];
                                        if (currentBranches){
                                            currentBranchArray = currentBranches.split(branchDelimeter);
                                        }

                                        if (branch){
                                             branchFragment = decodeURI(branch).split('%2F');

                                             branch = "<a href=\"" + branch + "\" target=\"_blank\">" + branchFragment.slice(-1)[0] + "</a>";
                                             currentBranchArray.push(branch);
                                             me.logger.log(branch);
                                        }
                                        if (currentBranchArray && currentBranchArray.length > 0){
                                            selectedRecord.set(branchField, currentBranchArray.join(branchDelimeter));
                                        }

                                        selectedRecord.save();
                                    } //end if branchLink
                                  } //end if confirm
                              }// end listeners
                         });  //create dialog
                      } //end _showMenu
                    } //end rowActionColumnConfig
                } //end gridConfig
            }); //end this.add
    },

    getBranchLinkField: function(){
      return this.getSetting('branchField');
    },

    getCreateBranchLink: function(){
      return this.getSetting('createBranchLink');
    },

    getBranchDelimeter: function(){
       return this.getSetting('branchDelimeter');
    },

    getQueryString: function(){
       var query = this.getSetting('queryString');
       if (!query || query.length === 0){
         query = "(ObjectID > 0)"
       }
       return Rally.data.wsapi.Filter.fromQueryString(query);
    },

    getPromptForBranchLink: function(){
      return this.getSetting('promptForBranchLink') === 'true' || this.getSetting('promptForBranchLink') === true;
    },

    _onWindowLoad: function(w){
         console.log('_onwindowload', w);
    },

    _getArtifactTypes: function(){
       return this.getSetting('artifactTypes');
    },

    getSettingsFields: function() {
        var labelAlign = 'top',
            labelWidth = 150;
        var check_box_margins = '5 0 5 0';
        return [{
          name: 'branchField',
          xtype: 'rallyfieldcombobox',
          model: 'PortfolioItem',
          anchor: '50%',
          fieldLabel: '<b>Branch Link(s) field</b><br/><span style="color:#999999;"><i>The branch link field must be a field of type TEXT to accomodate multiple branches.  The field must also exist on the Portfolio Item type as well as any other artifact types to allow for branches to be created from. If the field does not exist on the artifact type, then branch creations will not be launchable from the app for that artifact type.</i></span>',
          margin:3,
         labelAlign: labelAlign,
          _isNotHidden: function(field) {
              return field  && field.attributeDefinition &&
                  field.attributeDefinition.AttributeType &&
                  field.attributeDefinition.AttributeType === "TEXT";
          }
        },{
          name: 'createBranchLink',
          xtype: 'rallytextfield',
          fieldLabel: '<b>Link to create branch</b><br/><span style="color:#999999;"><i>The link needed to create a branch on the bitbucket server. {id} and {name} can be used to substitute attributes from the selected artifact.  The selected object FormattedID will populate the {id} and the Name will populate the {name} variable.</i></span>',
          emptyText: 'http://myBitbucketServer:7990/plugins/servlet/create-branch?issueKey={id}&issueType=Story&issueSummary={name}',
          margin: '3 70 3 3',
          anchor: '100%',
          //labelWidth: labelWidth,
          labelAlign: labelAlign
        }, {
            xtype: 'textarea',
            fieldLabel: '<b>Artifact Query</b><br/><span style="color:#999999;"><i>Note that if the grid is showing multiple artifact types, the query must apply to all artifact types.  If the query only applies to one artifact type, only that artifact type will be displayed in the grid.</i></span>',
            name: 'queryString',
            anchor: '100%',
            cls: 'query-field',
            margin: '0 70 0 0',
            labelAlign: labelAlign,
          //  labelWidth: labelWidth,
            plugins: [
                'rallyfieldvalidationui'
            ],
            validateOnBlur: false,
            validateOnChange: false,
            validator: function(value) {
                try {
                    if (value) {
                        Rally.data.wsapi.Filter.fromQueryString(value);
                    }
                    return true;
                } catch (e) {
                    return e.message;
                }
            }
        },{
            name: 'promptForBranchLink',
            xtype: 'rallycheckboxfield',
            boxLabelAlign: 'after',
            fieldLabel: '',
            margin: check_box_margins,
            boxLabel: 'Prompt for Branch Link<br/><span style="color:#999999;"><i>Prompt user to add the created branch link in the Branch Links field.  Only select this option if you do not have the Rally Branch Linker plugin in Bitbucket enabled.</i></span>'

        },{
            name: 'saveLog',
            xtype: 'rallycheckboxfield',
            boxLabelAlign: 'after',
            fieldLabel: '',
            margin: check_box_margins,
            boxLabel: 'Save Logging<br/><span style="color:#999999;"><i>Save last 100 lines of log for debugging.</i></span>'

        }];
    },

    getOptions: function() {
        var options = [
            {
                text: 'About...',
                handler: this._launchInfo,
                scope: this
            }
        ];

        return options;
    },

    _launchInfo: function() {
        if ( this.about_dialog ) { this.about_dialog.destroy(); }

        this.about_dialog = Ext.create('Rally.technicalservices.InfoLink',{
            showLog: this.getSetting('saveLog'),
            logger: this.logger
        });
    },

    isExternal: function(){
        return typeof(this.getAppId()) == 'undefined';
    }

});
